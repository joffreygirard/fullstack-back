import { AbstractController } from '../common/abstract.controller';
import { AbstractRouter } from '../common/abstract.router';
import { movieController } from './movie.controller';
import { IMovie, IMovieDto } from './movie.model';

class MovieRouter extends AbstractRouter<IMovie, IMovieDto> {

    protected get controller(): AbstractController<IMovie, IMovieDto> {
        return movieController;
    }
}

export const movieRouter = new MovieRouter().router;
