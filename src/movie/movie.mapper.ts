import moment from 'moment';
import { AbstractMapper } from '../common/abstract.mapper';
import { IMovie, IMovieDto } from './movie.model';

export class MovieMapper extends AbstractMapper<IMovie, IMovieDto> {

    modelToDto(model: IMovie): IMovieDto {
        return {
            id: model._id,
            title: model.title,
            year: model.year,
            imageUrl: model.imageUrl
        };
    }

    dtoToModel(dto: IMovieDto): IMovie {
        return {
            title: dto.title,
            year: dto.year,
            imageUrl: dto.imageUrl
        };
    }
}

export const movieMapper = new MovieMapper();
