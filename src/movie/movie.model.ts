import { IDto, IModel } from '../common/abstract.model';

export interface IMovie extends IModel {
    title: string;
    year: number;
    imageUrl: string;
}

export interface IMovieDto extends IDto {
    title: string;
    year: number;
    imageUrl: string;
}
