import { AbstractRepository } from '../common/abstract.repository';
import { IMovie } from './movie.model';

class MovieRepository extends AbstractRepository<IMovie> {
}

const collection = 'movie';
export const movieRepository = new MovieRepository(collection);
