import express from 'express';
import { characterRouter } from './character/character.router';
import { movieRouter } from './movie/movie.router';
import { routeNotFoundMiddleware } from "./middleware/route-not-found.middleware";

export const router = express.Router();
router.use('/characters', characterRouter);
router.use('/movies', movieRouter);
router.use(routeNotFoundMiddleware);
