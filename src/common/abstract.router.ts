import express from 'express';
import { AbstractController } from './abstract.controller';
import { IDto, IModel } from './abstract.model';
import { routeParamIdMiddleware } from '../middleware/route-param-id.middleware';

export abstract class AbstractRouter<M extends IModel, D extends IDto> {
    router = express.Router();

    protected abstract get controller(): AbstractController<M, D>;

    constructor() {
        this.configure();
    }

    protected configure(): void {
        this.router.get('/', (req, res, next) => this.controller.findAll(req, res, next));
        this.router.get('/:id', routeParamIdMiddleware, (req, res, next) => this.controller.get(req, res, next));
        this.router.post('/', (req, res, next) => this.controller.create(req, res, next));
        this.router.put('/:id', routeParamIdMiddleware, (req, res, next) => this.controller.update(req, res, next));
        this.router.delete('/:id', routeParamIdMiddleware, (req, res, next) => this.controller.remove(req, res, next));
    }
}
