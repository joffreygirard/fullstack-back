import { AbstractMapper } from './abstract.mapper';
import { IDto, IModel } from './abstract.model';
import { AbstractRepository } from './abstract.repository';
import { itemErrorHandler } from './error/error.mapper';

export abstract class AbstractService<M extends IModel, D extends IDto> {

    protected abstract get repository(): AbstractRepository<M>;
    protected abstract get mapper(): AbstractMapper<M, D>

    findAll(): Promise<D[]> {
        return this.repository.findAll()
            .then(models => this.mapper.modelsToDtos(models));
    }

    get(id: string): Promise<D> {
        return this.repository.get(id)
            .then(model => this.mapper.modelToDto(model))
            .catch(itemErrorHandler(id));
    }

    create(dto: D): Promise<D> {
        const character = this.mapper.dtoToModel(dto);
        return this.repository.create(character)
            .then(model => this.mapper.modelToDto(model));
    }

    update(id: string, dto: D): Promise<number> {
        const character = this.mapper.dtoToModel(dto);
        return this.repository.update(id, character)
            .then(result => {
                return result;
            })
            .catch(itemErrorHandler(id));
    }

    remove(id: string): Promise<number> {
        return this.repository.remove(id)
            .then(result => {
                return result;
            })
            .catch(itemErrorHandler(id));
    }
}
