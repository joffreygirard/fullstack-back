import { IDto, IModel } from './abstract.model';

export abstract class AbstractMapper<M extends IModel, D extends IDto> {

    abstract modelToDto(model: M): D;
    abstract dtoToModel(dto: D): M;

    modelsToDtos(models: M[]): D[] {
        return models.map(model => this.modelToDto(model));
    }

    dtosToModels(dtos: D[]): M[] {
        return dtos.map(dto => this.dtoToModel(dto));
    }
}
