import { NextFunction, Request, Response } from 'express';

export const logMiddleware = (request: Request, response: Response, next: NextFunction): void => {
    console.log(`METHOD : ${request.method} `
        + `| PATH : ${request.url} `
        + `| CONTENT-TYPE : ${request.headers["content-type"]}`);
    next();
}
