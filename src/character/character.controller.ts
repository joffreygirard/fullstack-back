import { AbstractController } from '../common/abstract.controller';
import { AbstractService } from '../common/abstract.service';
import { ICharacter, ICharacterDto } from './character.model';
import { characterService } from './character.service';

class CharacterController extends AbstractController<ICharacter, ICharacterDto> {

    protected get service(): AbstractService<ICharacter, ICharacterDto> {
        return characterService;
    }
}

export const characterController = new CharacterController();
