import { AbstractController } from '../common/abstract.controller';
import { AbstractRouter } from '../common/abstract.router';
import { characterController } from './character.controller';
import { ICharacter, ICharacterDto } from './character.model';

class CharacterRouter extends AbstractRouter<ICharacter, ICharacterDto> {

    protected get controller(): AbstractController<ICharacter, ICharacterDto> {
        return characterController;
    }
}

export const characterRouter = new CharacterRouter().router;
