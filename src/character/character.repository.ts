import { AbstractRepository } from '../common/abstract.repository';
import { ICharacter } from './character.model';

class CharacterRepository extends AbstractRepository<ICharacter> {
}

const collection = 'character';
export const characterRepository = new CharacterRepository(collection);
