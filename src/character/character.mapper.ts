import moment from 'moment';
import { AbstractMapper } from '../common/abstract.mapper';
import { ICharacter, ICharacterDto } from './character.model';

export class CharacterMapper extends AbstractMapper<ICharacter, ICharacterDto> {

    modelToDto(model: ICharacter): ICharacterDto {
        return {
            id: model._id,
            firstName: model.firstName,
            lastName: model.lastName,
            birthDate: model.birthDate
        };
    }

    dtoToModel(dto: ICharacterDto): ICharacter {
        return {
            firstName: dto.firstName,
            lastName: dto.lastName,
            birthDate: new Date(dto.birthDate)
        };
    }
}

export const characterMapper = new CharacterMapper();
