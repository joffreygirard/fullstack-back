import { IDto, IModel } from '../common/abstract.model';

export interface ICharacter extends IModel {
    firstName: string;
    lastName: string;
    birthDate: any;
}

export interface ICharacterDto extends IDto {
    firstName: string;
    lastName: string;
    birthDate: string;
}
